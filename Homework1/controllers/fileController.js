const fs = require('fs');
const path = './files';

const checkFilename = (filename) => {
  const filenameArray = filename.split('.');
  if (filenameArray.length === 2 && checkExtension(filenameArray[1])) {
    return true;
  } else {
    return false;
  }
}

const checkExtension = (extension) => {
  if (extension === 'log' || extension === 'txt' || extension === 'json' ||
    extension === 'yaml' || extension === 'xml' || extension === 'js') {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  getFiles(req, res) {
    res.set('Content-type', 'application/json');
    try {
      const files = fs.readdirSync(path);
      res.status(200).send({
        "message": "Success",
        "files": files
      });
    }
    catch {
      res.status(500).send({
        "message": "Server error"
      });
    }
  },

  createFile(req, res) {
    const filename = req.body.filename;
    const content = req.body.content;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    if (!filename) {
      res.status(400).send({
        "message": "Please specify 'filename' parameter"
      });
    } else if (!content) {
      res.status(400).send({
        "message": "Please specify 'content' parameter"
      });
    } else if (!checkFilename(filename)) {
      res.status(400).send({
        "message": "Incorrect filename"
      });
    } else if (fs.existsSync(filepath)) {
      res.status(400).send({
        "message": "File with filename '" + filename + "' has already exists"
      });
    } else {
      try {
        fs.writeFileSync(filepath, content);
        res.status(200).send({
          "message": "File created successfully"
        });
      }
      catch {
        res.status(500).send({
          "message": "Server error"
        });
      }
    }
  },

  getFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    if (!fs.existsSync(filepath)) {
      res.status(400).send({
        "message": "No file with '" + filename + "' filename found"
      });
    } else {
      try {
        const extension = filename.split('.')[1];
        const content = fs.readFileSync(filepath, { encoding: 'utf8', flag: 'r' });
        console.log(content);
        const uploadedDate = fs.statSync(filepath).birthtime;
        res.status(200).send({
          "message": "Success",
          "filename": filename,
          "content": content,
          "extension": extension,
          "uploadedDate": uploadedDate
        });
      } catch {
        res.status(500).send({
          "message": "Server error"
        });
      }
    }
  },

  updateFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    if (!fs.existsSync(filepath)) {
      res.status(400).send({
        "message": "No file with '" + filename + "' filename found"
      });
    } else {
      try {
        const content = req.body.content;
        fs.writeFileSync(filepath, content);
        res.status(200).send({
          "message": "File updated successfully"
        });
      } catch {
        res.status(500).send({
          "message": "Server error"
        });
      }
    }
  },

  deleteFile(req, res) {
    const filename = req.params.filename;
    const filepath = path + '/' + filename;
    res.set('Content-type', 'application/json');
    if (!fs.existsSync(filepath)) {
      res.status(400).send({
        "message": "No file with '" + filename + "' filename found"
      });
    } else {
      try {
        fs.rmSync(filepath);
        res.status(200).send({
          "message": "File deleted successfully"
        });
      } catch {
        res.status(500).send({
          "message": "Server error"
        });
      }
    }
  }
}